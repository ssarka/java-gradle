package com.gitlab.security_products.tests;

import java.security.Key;
import java.security.SecureRandom;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import java.sql.*;

public class App
{
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/EMP";
    static final String USER = "username";
    static final String PASS = "pass";
    private static final Integer fileLock = new Integer(1);
    public static void main( String[] args )
    {
        synchronized(fileLock) {
        }
        System.out.println( "Hello World!" );
    }

    public Boolean booleanMethod() {
        return null;
    }

    public String generateSecretToken() {
        Random r = new Random();
        return Long.toHexString(r.nextLong());
    }

    public byte[] mDigest(){
        try {
            MessageDigest md5Digest = MessageDigest.getInstance("MD5");
            md5Digest.update("password".getBytes());
            byte[] hashValue = md5Digest.digest();
            return hashValue;

        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);

        }
    }

    public Number[] covariantArray(){
        Number[] arr = new Integer[10];
        arr[0] = 1.0;
        return arr;
    }

    public int hashCode() {
        assert false : "hashCode not designed";
        return 42; // any arbitrary constant will do
    }

    public void connectDB(){
        Connection conn = null;
        Statement stmt = null;
        try{
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            stmt = conn.createStatement();
            String sql;
            String name = "john";
            sql = "SELECT id, first, last, age FROM Employees where first = "+ name;
            ResultSet rs = stmt.executeQuery(sql);
        }catch(SQLException se){
            se.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
        }
    }

}
